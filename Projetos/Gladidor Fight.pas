Program Gladiador_Fight;
Var NomeUTL, num2 :String;
var NomeCPU : array [0..10] of String;
Var AtaqueForteUTL, AtaqueMedioUTL, AtaqueFracoUTL, EscolhaMenuInicial, NVitorias, NDerrotas, VidaInicalUTL : integer;
Var num1, EscolhaCombateUTL, EscolhaCombateCPU, ResistenciaUTL, ResistenciaInicialUTL:integer;
var VidaUTL, melhoriaFinal : integer;
var AtaqueForteCPU, AtaqueMedioCPU, AtaqueFracoCPU, VidaCPU, ResistenciaCPU : integer;
Var melhoria, nivel : Integer;
//NomeUTL = Nome do Gladiador do Utilizador
//num2 = Nome final do Gladiador do cpu
//NomeCPU = Vetor de nomes disponiveis para o cpu
//AtaqueForteUTL, AtaqueMedioUTL, AtaqueFracoUTL = Niveis de Ataque do Utilizador
//AtaqueForteCPU, AtaqueMedioCPU, AtaqueFracoCPU = Niveis de Ataque do Computador
//VidaUTL = Vida do Utilizador
//VidaCPU = Vida do Computador
//EscolhaMenuInicial = Escolha no menu entre "Combater, Modar o nome do gladiador, Modar gladiador e Sair
//num1 = Numero Aleat�rio para a escolha de nomes do cpu
//EscolhaCombateUTL = Escolha de ataque durante o combate do Utilizador
//EscolhaCombateUTL = Escolha de ataque durante o combate do Computador
//NVitorias e NDerrotas = N�mero de Vit�rias e Derrotas
//VidaInicalUTL = Vida Inicial do Utilizador
//Melhoria = Valor aleat�ria da melhoria do lutador ap�s uma vit�ria

Begin
//Informa��o do Criador
	writeln('--------------------------------------------------------------------------------');
	Writeln('---------------GAME MAKE BY -> FL�VIO SILVA <- GAME MAKE BY---------------------');
	writeln('--------------------------------------------------------------------------------');
	
	//Nome do Game
	writeln('--------------------------------------------------------------------------------');
	Writeln('------------------------------GLADEADOR FIGHT-----------------------------------');
	writeln('--------------------------------------------------------------------------------');
	
//Escolha Alea�tria de Vida e Ataques do Utilizador 
		VidaInicalUTL:=random(60); 
		AtaqueForteUTL:=random(60);
		AtaqueMedioUTL:=random(40); 
		AtaqueFracoUTL:=random(25);
    ResistenciaInicialUTL:=Random(20);

//Nomes para o CPU disponiveis
		NomeCPU[1]:='Ivanildo Apolinario';	
		NomeCPU[1]:='Atilio';
		NomeCPU[2]:='Presunto';
		NomeCPU[3]:='Andr�';
		NomeCPU[4]:='Conner';
		NomeCPU[5]:='Ramiro';
		NomeCPU[6]:='Xavier';
		NomeCPU[7]:='Alfredo';
		NomeCPU[8]:='Jo�o';
		NomeCPU[9]:='Jos�';
		NomeCPU[10]:='Ant�nio';     

 //Apresenta��o Inicial
  	writeln('--------------------------------------------------------------------------------');
 		Writeln('Neste programa tu �s um lutador de Gladiador!');
  	writeln('Ter�s de lutar contra outros lutadores para seres um Verdadeiro GLADEADOR!');
  	writeln('Cada batalha vencida, vais melhorando e evoluindo'); 
  	writeln('--------------------------------------------------------------------------------');
  	
  	//primeira vez que pede o nome do Gladiador
  	Write('Diz o nome do teu gladiador: ');
  	Read(NomeUTL);
  	writeln('');
  	
repeat
//Igualiza��o de vidas e Resistencia
VidaUTL:=VidaInicalUTL;
ResistenciaUTL:=ResistenciaInicialUTL;

//Apresenta��o dos resultados da vida e dos Ataques do Utilizador
  	writeln('');
  	Writeln('Nome Gladiador-------------------------', NomeUTL);
  	Writeln('Vida Atual-----------------------------', VidaUTL,'/100');
  	writeln('Poder de Ataque Forte------------------', AtaqueForteUTL,'/100');
  	writeln('Poder de Ataque M�dio------------------', AtaqueMedioUTL,'/100');
		writeln('Poder de Ataque Fraco------------------', AtaqueFracoUTL,'/100');
		writeln('Resistencia Atual----------------------', ResistenciaUTL,'/50');
		Writeln('N�mero de Vit�rias---------------------', NVitorias);
		writeln('N�mero de Derrotas---------------------', NDerrotas);
		writeln('');
		
		//Menu Inicial
		writeln('--------------------------------------------------------------------------------');
  	Writeln('Agora vais come�ar verdadeiramente o jogo e o desafio!');
    Writeln('Escolhe a op��o que quiseres:');
    Writeln('Combater--------------1');   //Combater
    Writeln('Mudar Nome------------2');  //Apenas Modar o nome do Gladiador
    Writeln('Novo Gladiador--------3');	//Novo Lutador Aleat�rio, com vida e ataques aleat�rios novos
    Writeln('Sair------------------4');//Sair do Programa
    writeln('');
    write  ('A tua escolha �: ');
    readln(EscolhaMenuInicial);
    writeln('--------------------------------------------------------------------------------');
    writeln('');
    
  case EscolhaMenuInicial of
  
      1 : //Escolha do combate
			begin 
			writeln('');
			writeln('Escolheste Combater!');
			writeln('--------------------------------------------------------------------------------');
			writeln('Aqui um advers�rio � tua altura');
			writeln('Lembra-te que o teu objetivo � n�o morrer e derrotar o advers�rio!');
			writeln('');
			
			//Escolha de Nome, Vida e Ataque aleat�rios para o CPU
			num1:=random(10);
			VidaCPU:=random(99);
			AtaqueForteCPU:=random(99);
		  AtaqueMedioCPU:=random(74); 
		  AtaqueFracoCPU:=random(49);
		  ResistenciaCPU:=random(14);
			num2:=NomeCPU[num1];
			
			//Evitar Seja possivel calhar 0
			VidaCPU:=VidaCPU+1;
			AtaqueForteCPU:=AtaqueForteCPU+1;
			AtaqueMedioCPU:=AtaqueMedioCPU+1;
			AtaqueFracoCPU:=AtaqueFracoCPU+1;
			ResistenciaCPU:=ResistenciaCPU+1;
										
			  //Inicio do Combate
	repeat
			//Informa��o do Gladiador do CPU       
			Writeln('Nome Gladiador Computador--------------', num2);
  		Writeln('Vida Atual-----------------------------', VidaCPU,'/100');
  		writeln('Poder de Ataque Forte------------------', AtaqueForteCPU,'/100');
  		writeln('Poder de Ataque M�dio------------------', AtaqueMedioCPU,'/100');
			writeln('Poder de Ataque Fraco------------------', AtaqueFracoCPU,'/100');
			writeln('Resist�ncia Atual----------------------', ResistenciaCPU,'/50');
			writeln('--------------------------------------------------------------------------------');
			
			//Informa��o do Gladiador do Utilizador
			writeln(' ');
  		Writeln('Nome Gladiador-------------------------', NomeUTL);
  		Writeln('Vida Atual-----------------------------', VidaUTL,'/100');
  		writeln('Poder de Ataque Forte------------------', AtaqueForteUTL,'/100');
  		writeln('Poder de Ataque M�dio------------------', AtaqueMedioUTL,'/100');
			writeln('Poder de Ataque Fraco------------------', AtaqueFracoUTL,'/100');
			writeln('Resist�ncia Atual----------------------', ResistenciaUTL,'/50');
			writeln('--------------------------------------------------------------------------------');
			writeln('');
			
			//Apresenta��o dos Ataques disponiveis
			Writeln('Escolhe a op��o que quiseres:');
    	Writeln('Ataque Forte--------------1');
    	Writeln('-4 de Resistencia----------');
    	writeln('');
    	Writeln('Ataque M�dio--------------2');
    	Writeln('-2 de Resistencia----------');
    	writeln('');
    	Writeln('Ataque Fraco--------------3');
    	Writeln('-1 de Resistencia----------');
    	writeln('');
    	Writeln('N�o fazer nada!-----------4');
    	writeln('');
    	write  ('A tua escolhe �: ');
			readln(EscolhaCombateUTL);
			writeln('');
			writeln('----------------------------------');
				
		// Escolha de Ataque do Utilizador
		case EscolhaCombateUTL of
		
			1 :   //Ataque Forte
		begin 
			if (ResistenciaUTL = 4) or (ResistenciaUTL > 4) then
		 begin
			writeln('Fizeste um Ataque Forte');
			Vidacpu:=VidaCPU-AtaqueForteUTL;
			writeln('');
			writeln('O computador perdeu: ', AtaqueForteUTL, ' de vida.');
			writeln('');
			ResistenciaUTL:=ResistenciaUTL-4;
			writeln('Perdes -4 de rest�ncia');
			writeln('');
		 end;
			
			if (ResistenciaUTL < 4) then
			begin
			  EscolhaCombateUTL :=2;
			  writeln('N�o tens engergia suficiente para um ataque Forte!');
				writeln('');
				end;
		end;
			
			2 : //Ataque M�dio
		begin 
			if (ResistenciaUTL = 4) or (ResistenciaUTL > 4) then
		 begin
			writeln('Fizeste um Ataque M�dio');
			Vidacpu:=VidaCPU-AtaqueMedioUTL;
			writeln('');
			writeln('O computador perdeu: ', AtaqueMedioUTL, ' de vida.');
			writeln('');
			ResistenciaUTL:=ResistenciaUTL-2;
			writeln('Perdes -2 de rest�ncia');
			writeln('');
		 end;
			
			if (ResistenciaUTL < 2) then
			begin
			  EscolhaCombateUTL :=3;
			  writeln('N�o tens engergia suficiente para um ataque M�dio!');
				writeln('');
				end;
		end;
			
			3 :  //Ataque Fraco
		begin 
			writeln('Fizeste um Ataque Fraco');
			Vidacpu:=VidaCPU-AtaqueFracoUTL;
			writeln('');
			ResistenciaUTL:=ResistenciaUTL-1;
			writeln('O computador perdeu: ', AtaqueFracoUTL, ' de vida-');
			writeln('');
			writeln('Perdes -1 de rest�ncia');
			writeln('');
			end;
			
			4: //N�o fazer nada
			begin
			writeln('Escolheste n�o fazer nada!');
			end;
		end;
		
		writeln('--------------------------------------------------------------------------------');
		writeln('-------------------------------Vez do computador--------------------------------');
		writeln('--------------------------------------------------------------------------------');
		
		// Escolha de Ataque do CPU
	   EscolhaCombateCPU:=(Random(3));
	   
	   case EscolhaCombateCPU of
	   1: //Ataque Forte CPU
	   begin
	   if (ResistenciaCPU = 4) or (ResistenciaCPU > 4) then
	   	begin
	  	writeln('O computador Fez um Ataque Forte de: ', AtaqueForteCPU);
			vidaUTL:=VidaUTL-AtaqueForteCPU;
			writeln('');
			writeln('Perdeste: ',AtaqueForteCPU , ' de vida');
			writeln('');
			ResistenciaCPU:=ResistenciaCPU-4;
			writeln('O computador perdeu -4 de rest�ncia');
			writeln('');
			end
			
			else
			EscolhaCombateCpu:=2;
		 end;
		 
		 2 : //Ataque M�dio CPU
			begin 
			if (ResistenciaCPU = 2) or (ResistenciaCPU > 2) then
			begin
			writeln('O computador Fez um Ataque M�dio de: ', AtaqueMedioCPU);
			vidaUTL:=VidaUTL-AtaqueMedioCPU;
			writeln('');
			writeln('Perdeste: ',AtaqueMedioCPU , ' de vida');
			writeln('');
			ResistenciaCPU:=ResistenciaCPU-2;
			writeln('O computador perdeu -2 de rest�ncia');
			writeln('');
			end
			
			else
			EscolhaCombateCpu:=3;
			end;
			
			3 :  //Ataque Fraco CPU
			begin 
			writeln('O computador Fez um Ataque Fraco de: ', AtaqueFracoCPU);
			vidaUTL:=VidaUTL-AtaqueFracoCPU;
			writeln('');
			writeln('Perdeste: ',AtaqueFracoCPU , ' de vida');
			writeln('');
			ResistenciaCPU:=ResistenciaCPU-1;
			writeln('O computador perdeu -1 de rest�ncia');
			writeln('');
			end;
		end;
		
				
		writeln('--------------------------------------------------------------------------------');
		writeln('----------------------------Aumento de Resistencias-----------------------------');
		writeln('--------------------------------------------------------------------------------');
		
		ResistenciaCPU:=ResistenciaCPU+1;
		ResistenciaUTL:=ResistenciaUTL+1;
		writeln('O computador ganhou +1 de rest�ncia');
		Writeln('');
		writeln('Ganhaste +1 de rest�ncia');
		writeln('');
		
	//Fim de combate	
	 until (VidaCPU=0) or (VidaUTL=0) or (VidaCPU<0) or (VidaUTL<0);
	 
	 //Aumento de Vit�rias e Derrotas
	   if  (VidaCPU=0) or (VidaCPU<0) then
	 begin
	 writeln('Venceste Muito bem!! E o teu Gladeador Evoluiu!');
	 writeln('');
		NVitorias:=NVitorias+1;
		
		 //Aumento do poder da vida, e Ataques
		 melhoria:=random(2);
		 melhoriaFinal:=melhoria*2;
			VidaInicalUTL:=VidaInicalUTL+melhoriaFinal;
			AtaqueForteUTL:=AtaqueForteUTL+melhoriaFinal;
			AtaqueMedioUTL:=AtaqueMedioUTL+melhoriaFinal;
			AtaqueFracoUTL:=AtaqueFracoUTL+melhoriaFinal;
			ResistenciaInicialUTL:= ResistenciaInicialUTL+melhoriaFinal;
			writeln('Melhoras-te: ',melhoriaFinal, ' na vida, nos ataques e na resistencia.');
	 		writeln('');
		 end
		 
		  //Derrota
		else If  (VidaUTL=0) or (VidaUTL<0) then
		begin
		writeln('Perdes-te, tenta novamente, mas n�o desistas nunca!');
		writeln('');
		NDerrotas:=NDerrotas+1;
		 end;
		 
writeln('--------------------------------------------------------------------------------');
		 
		 //Impossibilidade de Qualquer poder maior que 100
		 if  (VidaInicalUTL= 100) or (VidaInicalUTL>100) then
		 VidaInicalUTL:=100;
		 
		 if  (AtaqueForteUTL= 100) or (AtaqueForteUTL>100) then
		 AtaqueForteUTL:=100;
		 
		 if  (AtaqueMedioUTL= 100) or (AtaqueMedioUTL>100) then
		 AtaqueMedioUTL:=100;
		 
		 if  (AtaqueFracoUTL= 100) or (AtaqueFracoUTL>100) then
		 AtaqueFracoUTL:=100;
		 
		 if (ResistenciaInicialUTL=100) or (ResistenciaInicialUTL>100) then
		 ResistenciaInicialUTL:=100;
					end;
			
      2 :  //Mudar Nome de Gladiador
			begin    
			writeln('');
			writeln('Escolheste Mudar o Nome de Gladiador!');
			readln(NomeUTL);		
			end;
			
			3 :   //Mudar de Gladiador
			begin                                                       
			writeln('');
			writeln('Escolheste Mudar de Gladiador!');
			VidaInicalUTL:=random(25); 
			AtaqueForteUTL:=random(25);
			AtaqueMedioUTL:=random(15); 
			AtaqueFracoUTL:=random(7);
			ResistenciaInicialUTL:=random(10);
			end;
			
			4 :   //Sair
			begin 
			writeln('');
			writeln('Escolheste Sair');
			end;
	end;
	
	//fim da Aplica��o
until EscolhaMenuInicial=4;
End.